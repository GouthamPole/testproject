using FluentAssertions;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace TestProject
{
    internal class Tests
    {
        public IWebDriver driver;

        [SetUp]
        public void startBrowser()
        {

            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();

        }

        //Action class
        [Test]
        [Category("Regression")]
        public void test()
        {
            Actions actions = new Actions(driver);
            actions.Perform();
            driver.Url = "http://www.google.co.in";
            Console.WriteLine(driver.Title);
            IWebElement searchBox = driver.FindElement(By.Name("q"));
            searchBox.SendKeys("c# selenium");
            searchBox.SendKeys(Keys.Enter);
            driver.FindElement(By.Name("q")).Text
                  .Should()
                  .Be("c# selenium");

        }
        [Test]
        [Category("Regression")]
        public void TestCheckboxes()
        {
            driver.Url = "https://the-internet.herokuapp.com/checkboxes";
            var checkboxes = driver.FindElements(By.XPath("//form/input")).ToList();
            foreach (var checkbox in checkboxes)
            {
                if (!checkbox.Selected)
                    checkbox.Click();

            }
            Assert.AreEqual(true, checkboxes[0].Enabled);

        }

        [Test]
        public void DatePicker()
        {
            driver.Url = "http://jqueryui.com/datepicker/";
            driver.SwitchTo().Frame(0);

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

            driver.FindElement(By.Id("datepicker")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);

            //Click on next so that we will be in next month
            driver.FindElement(By.XPath(".//*[@id='ui-datepicker-div']/div/a[2]/span")).Click();
            IWebElement dateWidget = driver.FindElement(By.Id("ui-datepicker-div"));

            var rows = dateWidget.FindElements(By.TagName("tr"));
            var columns = dateWidget.FindElements(By.TagName("td"));

            foreach (var cell in columns)
            {

                //Select 10th Date
                if (cell.Text.Equals("10"))
                {
                    cell.FindElement(By.LinkText("10")).Click();
                    break;
                }
            }

        }

        [Test]
        public void TestWindows()
        {
            driver.Url = "https://the-internet.herokuapp.com/windows";
            IWebElement linkBtn = driver.FindElement(By.LinkText("Click Here"));
            linkBtn.Click();
            var windows = driver.WindowHandles;
            foreach (var win in windows)
            {
                Console.WriteLine(driver.SwitchTo().Window(win).Title);
            }

        }
        [Test]
        [Category("Regression")]
        public void TestFrames()
        {
            driver.Url = "http://the-internet.herokuapp.com/nested_frames";
            driver.SwitchTo().Frame("frame-bottom");
            IWebElement element = driver.FindElement(By.XPath("//body"));
            Assert.AreEqual("BOTTOM", element.Text);
            driver.SwitchTo().ParentFrame();
            driver.SwitchTo().Frame("frame-top");
            driver.SwitchTo().Frame("frame-middle");
            element = driver.FindElement(By.XPath("//body"));
        }

        [Test]
        public void TestIFrames()
        {
            driver.Url = "http://the-internet.herokuapp.com/tinymce";
            driver.SwitchTo().Frame("mce_0_ifr");
            IWebElement text = driver.FindElement(By.Id("tinymce"));
            text.SendKeys(Keys.Control + "a");
            text.SendKeys("entering the data...");
            text.SendKeys(Keys.Control + "a");
            driver.SwitchTo().DefaultContent();
            driver.FindElement(By.XPath("//button[@title='Bold']")).Click();
        }
        [Test]
        public void TestDropdowns()
        {
            driver.Url = "https://the-internet.herokuapp.com/dropdown";
            IWebElement element = driver.FindElement(By.Id("dropdown"));
            SelectElement dropDown = new SelectElement(element);
            dropDown.SelectByIndex(1);
            Console.WriteLine(dropDown.SelectedOption.Text);
        }

        [Test]
        public void TestFileUpload()
        {
            driver.Url = "https://the-internet.herokuapp.com/upload";
            string fileName = "test.txt";
            string filePath = @"C:\Users\Goutham_Pole\source\repos\SeleniumBasics\" + fileName;
            IWebElement chooseFileBtn = driver.FindElement(By.Id("file-upload"));
            Actions actions = new Actions(driver);
            actions.MoveToElement(chooseFileBtn).Click().Perform();
            /*System.Windows.Forms. SendKeys.SendWait(filePath);
            System.Windows.Forms.SendKeys.SendWait("{ENTER}");*/
            driver.FindElement(By.Id("file-submit")).Click();
            string addedFile = driver.FindElement(By.Id("uploaded-files")).Text;
            Assert.AreEqual(fileName, addedFile);

        }


        [TearDown]
        public void closeBrowser()
        {
            driver.Close();

        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            /*if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(@"C:\TEMP\Screenshot.jpg", ImageFormat.Jpeg);
            }*/
        }
    }
}
